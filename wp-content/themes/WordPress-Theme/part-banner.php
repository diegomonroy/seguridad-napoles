<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'servicios', 'vigilancia-fija-movil-con-armas-y-sin-armas', 'escolta-a-personas-vehiculos-y-mercancias', 'vigilancia-canina', 'seguridad-electronica', 'seguridad-con-apoyo-tecnologico', 'servicios-conexos-de-asesoria-consultoria-e-investigacion-en-seguridad-privada' ) ) ) : dynamic_sidebar( 'banner_servicios' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->