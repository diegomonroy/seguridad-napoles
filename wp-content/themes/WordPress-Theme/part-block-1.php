<!-- Begin Block 1 -->
	<section class="block_1" data-wow-delay="0.5s">
		<div class="row align-center">
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'block_1' ); ?>
			</div>
		</div>
	</section>
	<section class="block_1" data-wow-delay="0.5s">
		<div class="row align-center">
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'block_1_2' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 1 -->