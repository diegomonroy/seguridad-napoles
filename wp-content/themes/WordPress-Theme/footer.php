		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( is_front_page() || is_page( array( 'clientes', 'proveedores', 'contacto', 'pqrs' ) ) ) : get_template_part( 'part', 'block-2' ); endif; ?>
		<?php if ( is_front_page() || is_page( array( 'quienes-somos', 'politicas', 'valores-corporativos', 'certificaciones', 'servicios', 'vigilancia-fija-movil-con-armas-y-sin-armas', 'escolta-a-personas-vehiculos-y-mercancias', 'vigilancia-canina', 'seguridad-electronica', 'seguridad-con-apoyo-tecnologico', 'servicios-conexos-de-asesoria-consultoria-e-investigacion-en-seguridad-privada' ) ) ) : get_template_part( 'part', 'block-3' ); endif; ?>
		<?php if ( is_front_page() || is_page( array( 'quienes-somos', 'politicas', 'valores-corporativos', 'certificaciones', 'servicios', 'vigilancia-fija-movil-con-armas-y-sin-armas', 'escolta-a-personas-vehiculos-y-mercancias', 'vigilancia-canina', 'seguridad-electronica', 'seguridad-con-apoyo-tecnologico', 'servicios-conexos-de-asesoria-consultoria-e-investigacion-en-seguridad-privada', 'proveedores', 'contacto', 'pqrs' ) ) ) : get_template_part( 'part', 'block-4' ); endif; ?>
		<?php get_template_part( 'part', 'block-5' ); ?>
		<?php get_template_part( 'part', 'block-6' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php dynamic_sidebar( 'pse' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>